# See following for more information: https://github.com/johntfoster/dotfiles 

# Identify OS and Machine -----------------------------------------
export OS=`uname -s | sed -e 's/  */-/g;y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/'`
export OSVERSION=`uname -r`; OSVERSION=`expr "$OSVERSION" : '[^0-9]*\([0-9]*\.[0-9]*\)'`
export MACHINE=`uname -m | sed -e 's/  */-/g;y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/'`
export PLATFORM="$MACHINE-$OS-$OSVERSION"
# Note, default OS is assumed to be OSX

# OS Specific Environment ------------------------------------------------------------
if [ "$OS" = "darwin" ] ; then
  export PATH=/usr/local/bin:/usr/local/sbin:$PATH  
  export PATH=/usr/local/screen/bin:$PATH  
  export PATH=/usr/local/emu/current/bin:$PATH  
  export PATH="/usr/texbin:$PATH"
  export PATH=/usr/local/Peridigm/bin:$PATH  
  export PATH=/usr/local/dakota/bin:$PATH  
  export PYTRILINOS_DIR=/usr/local/pytrilinos
  export TRILINOS_DIR=/usr/local/trilinos
  export PATH=$TRILINOS_DIR/bin:$PATH  
  export BYOBU_PREFIX=$(brew --prefix)
  source /usr/local/share/python/virtualenvwrapper.sh
  source /usr/local/opt/autoenv/activate.sh
fi


# Machine Specific Environments ------------------------------------
if [ "$(hostname)" == "shamu.coe.utsa.edu" ]; then
    export MODULEPATH=/share/apps/Modules/3.2.6/modulefiles
fi

# add your bin folder to the path, if you have it.  It's a good place to add all your scripts
if [ -d ~/bin ]; then
	export PATH=~/bin:$PATH  
fi

# Load in .bashrc -------------------------------------------------
source ~/.bashrc

# Hello Messsage --------------------------------------------------
echo -e "Kernel Information: " `uname -smr`
echo -e "`bash --version`"
echo -ne "Uptime: "; uptime
echo -ne "Server time is: "; date


# Notes: ----------------------------------------------------------
# When you start an interactive shell (log in, open terminal or iTerm in OS X, 
# or create a new tab in iTerm) the following files are read and run, in this order:
#     profile
#     bashrc
#     .bash_profile
#     .bashrc (only because this file is run (sourced) in .bash_profile)
#
# When an interactive shell, that is not a login shell, is started 
# (when you run "bash" from inside a shell, or when you start a shell in 
# xwindows [xterm/gnome-terminal/etc] ) the following files are read and executed, 
# in this order:
#     bashrc
#     .bashrc
